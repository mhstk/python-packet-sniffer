import struct
import socket
import binascii
from packet import Packet


OUTPUT_LOG = './output/log.txt'
OUTPUT_STAT = './output/statistic.txt'

class Sniffer:
    def __init__(self):
        self.num_packets = 0
        self.num_ipv4 = 0
        self.num_fragmenteds = 0
        self.num_tcp = 0
        self.num_udp = 0
        self.num_icmp = 0
        self.num_http = 0
        self.num_https = 0
        self.num_dns = 0
        

        self.ip_packets = {}
        self.used_ports = {}
        self.most_used_ports = []
        self.max_packet_size = None
        self.min_packet_size = None
        self.avg_packet_size = None

        
    # def start(self):
    #     conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))


    #     while True:
    #         #maximum tcp size is 64k and nearest number with power of 2 is 2^16.
    #         data, addr = conn.recvfrom(int(pow(2,16)))
    #         dest_mac, src_mac, protocol, data = self.unpack_ethernet(data)
    #         print(src_mac,dest_mac,protocol)
    #         version, length, fragment, ttl, protocol, header_checksum, src_ip, dest_ip, data = self.unpack_ip(data)
    #         print(src_ip, dest_ip, protocol)
    #         print(self.is_fragmented(fragment))
    #         self.transfer_layer(protocol, data)
    #         print("***")

    # def unpack_ethernet(self, frame):
    #     dest, src, ip_protocol = struct.unpack('! 6s 6s 2s' , frame[:14])
    #     return self.bytes_to_mac(dest), self.bytes_to_mac(src), int.from_bytes(ip_protocol, byteorder='little'), frame[14:]


    # def bytes_to_mac(self, bytes_address):
    #     str_addr = ''
    #     for i in bytes_address:
    #         str_addr += '{:02x}'.format(i) + ':'
    #     str_addr = str_addr[:-1]
    #     return str_addr

    # def unpack_ip(self, data):
    #     version_and_header_length, total_length, fragment,  ttl, protocol, header_checksum, src, dest = \
    #         struct.unpack('! 1s 1x 2s 2x 2s B B 2s 4s 4s', data[:20])
    #     version_and_header_length = version_and_header_length[0]
    #     version = int(version_and_header_length >> 4)
    #     header_length = int(version_and_header_length & 0b00001111) * 4
    #     total_length = int.from_bytes(total_length, byteorder='big')
    #     return version, total_length, fragment, ttl, protocol, header_checksum, \
    #         self.bytes_to_ipv4(src), self.bytes_to_ipv4(dest), data[header_length:]



    # def bytes_to_ipv4(self, bytes_address):
    #     str_addr = ''
    #     for i in bytes_address:
    #         str_addr += str(i) + '.'
    #     str_addr = str_addr[:-1]
    #     return str_addr


    # def is_fragmented(self, bytes):
    #     print('{:08b}'.format(bytes[0]))
    #     flagD = int(bytes[0] >> 7)
    #     return True if flagD == 0 else False


    # def transfer_layer(self,protocol,  data):
    #     #ICMP
    #     if protocol == 1:
    #         pass
    #     #TCP
    #     elif protocol == 6:
    #         src_port, dest_port, data = self.unpack_tcp(data)
    #         print(src_port, dest_port)
    #     #UDP
    #     elif protocol == 17:
    #         pass



    # def unpack_tcp(self, data):
    #     src_port, dest_port = struct.unpack('! H H' , data[:4])
    #     return src_port, dest_port, data[30:]



    def start(self):
        conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))


        while True:
            #maximum tcp size is 64k and nearest number with power of 2 is 2^16.
            data, addr = conn.recvfrom(int(pow(2,16)))
            p = Packet(data)
            p.print()
            self.update_statistic(p)
            # self.print_statistic()
            self.save_statistic()
            print('-----------------------------------------------------------------------------------------------------')

    def update_statistic(self, packet):
        self.num_packets += 1
        #ipv4
        if packet.ethernet_layer.protocol == b'\x08\x00':
            self.ip_packet_statistic(packet.ip_layer.src_ip)
            self.num_ipv4 += 1
            if packet.ip_layer.is_fragmented(): self.num_fragmenteds += 1
            if packet.ip_layer.protocol == 1:
                self.num_icmp += 1
            elif packet.ip_layer.protocol == 6:
                
                self.port_statistic(packet.transport_layer.src_port)
                self.port_statistic(packet.transport_layer.dest_port)
                

                self.num_tcp += 1
                if packet.transport_layer.is_http():
                    self.num_http += 1
                if packet.transport_layer.is_https():
                    self.num_https += 1
            elif packet.ip_layer.protocol == 17:
                self.port_statistic(packet.transport_layer.src_port)
                self.port_statistic(packet.transport_layer.dest_port)
                self.num_udp += 1
                if packet.transport_layer.is_dns():
                    self.num_dns += 1
            
        self.packet_length_statistic(packet)
        try:
            self.most_used_ports = sorted(self.used_ports.items(), key=lambda x: x[1], reverse=True)[:10]
        except:
            pass



    def packet_length_statistic(self, packet):
        if not self.max_packet_size:
            self.max_packet_size = self.min_packet_size = self.avg_packet_size = packet.packet_size
        else:
            if packet.packet_size > self.max_packet_size:
                self.max_packet_size = packet.packet_size
            if packet.packet_size < self.min_packet_size:
                self.min_packet_size = packet.packet_size
            self.avg_packet_size += (packet.packet_size - self.avg_packet_size)/(self.num_packets)


    def port_statistic(self, port):
        if port in self.used_ports:
            self.used_ports[port] += 1
        else:
            self.used_ports[port] = 1

    def ip_packet_statistic(self, ip):
        if ip in self.ip_packets:
            self.ip_packets[ip] += 1
        else:
            self.ip_packets[ip] = 1


    def save_statistic(self):
        with open(OUTPUT_STAT, 'w') as fw:
            fw.write(self.statistic())


    def statistic(self):
        out = ''
        out += f'packets: {self.num_packets}\n'
        out += f'ipv4: {self.num_ipv4}\n'
        out += f'fragmented: {self.num_fragmenteds}\n'
        out += f'icmp: {self.num_icmp}\n'
        out += f'tcp: {self.num_tcp}\n'
        out += f'udp: {self.num_udp}\n'
        out += f'http: {self.num_http}\n'
        out += f'https: {self.num_https}\n'
        out += f'dns: {self.num_dns}\n'
        out += '\n'
        out += 'max_packet_length: '+ str(self.max_packet_size) + '\n'
        out += 'min_packet_length: '+ str(self.min_packet_size) + '\n'
        out += 'avg_packet_length: '+ str(self.avg_packet_size) + '\n'
        out += '\nTop 10 used ports:\n'
        out += self.str_port_used()
        out += '\n\nSource ip used:\n'
        out += self.str_ip_used()
        return out

    def str_port_used(self):
        out = ''
        for port, val in self.most_used_ports:
            out += f'port: {port}\t times: {val}' + '\n'
        return out
    
    def str_ip_used(self):
        out = ''
        for ip, val in sorted(self.ip_packets.items(), key= lambda x: x[1], reverse=True):
            out += f'ip: {ip}\t times: {val}' + '\n'
        return out


    def print_statistic(self):
        print(f'packets: {self.num_packets}')
        print(f'ipv4: {self.num_ipv4}')
        print(f'fragmented: {self.num_fragmenteds}')
        print(f'icmp: {self.num_icmp}')
        print(f'tcp: {self.num_tcp}')
        print(f'udp: {self.num_udp}')
        print(f'http: {self.num_http}')
        print(f'https: {self.num_https}')
        print(f'dns: {self.num_dns}')
        print('\n')
        print('max: ', self.max_packet_size)
        print('min: ', self.min_packet_size)
        print('avg: ', self.avg_packet_size)
        print('\n')
        print(self.most_used_ports)
        







if __name__ == '__main__':
    s = Sniffer()
    s.start()