import struct
from protocols import *

class Packet:
    def __init__(self, data):
        self.packet_size = len(data)
        self.ethernet_layer = Ethernet_layer(*self.unpack_ethernet(data))
        self.ip_layer = None
        self.transport_layer = None
        self.application_layer = None
        if self.ethernet_layer.protocol == b'\x08\x00':
            self.ip_layer = IPv4_layer(*self.unpack_ip(self.ethernet_layer.data))
            self.transport_layer, self.application_layer = self.unpack_transport_layer(self.ip_layer.protocol, self.ip_layer.data)


    def print(self):
        print(f'Packet length: {self.packet_size}')
        print()
        self.ethernet_layer.print()
        print()
        if self.ip_layer:
            self.ip_layer.print()
        print()
        if self.transport_layer:
            self.transport_layer.print()
            print()
        if self.application_layer:
            self.application_layer.print()
            print()


    def unpack_ethernet(self, frame):
        dest, src, ip_protocol = struct.unpack('! 6s 6s 2s' , frame[:14])
        return self.bytes_to_mac(dest), self.bytes_to_mac(src), ip_protocol, frame[14:]


    def bytes_to_mac(self, bytes_address):
        str_addr = ''
        for i in bytes_address:
            str_addr += '{:02x}'.format(i) + ':'
        str_addr = str_addr[:-1]
        return str_addr


    def unpack_ip(self, data):
        version_and_header_length, total_length, fragment,  ttl, protocol, header_checksum, src, dest = \
            struct.unpack('! 1s 1x 2s 2x 2s B B 2s 4s 4s', data[:20])
        version_and_header_length = version_and_header_length[0]
        version = int(version_and_header_length >> 4)
        header_length = int(version_and_header_length & 0b00001111) * 4
        total_length = int.from_bytes(total_length, byteorder='big')
        return version, total_length, fragment, ttl, protocol, header_checksum, \
            self.bytes_to_ipv4(src), self.bytes_to_ipv4(dest), data[header_length:]


    def bytes_to_ipv4(self, bytes_address):
        str_addr = ''
        for i in bytes_address:
            str_addr += str(i) + '.'
        str_addr = str_addr[:-1]
        return str_addr



    def unpack_transport_layer(self, protocol,  data):
        transport_layer = None
        application_layer = None
        #ICMP
        if protocol == 1:
            transport_layer = ICMP(*self.unpack_icmp(data))
        #TCP
        elif protocol == 6:
            transport_layer = TCP(*self.unpack_tcp(data))
            #HTTP
            if transport_layer.is_http():
                if transport_layer.src_port == 80:
                    application_layer = HTTP(transport_layer.data, response=True)
                else:
                    application_layer = HTTP(transport_layer.data, response=False)

                # application_layer = HTTP(transport_layer.data, False)
            #HTTPS
            if transport_layer.is_https():
                application_layer = HTTPS(transport_layer.data)
            

        #UDP
        elif protocol == 17:
            transport_layer = UDP(*self.unpack_udp(data))
            #DNS
            if transport_layer.is_dns():
                application_layer = DNS(*self.unpack_dns(transport_layer.data))
        return transport_layer, application_layer


    def unpack_tcp(self, data):
        src_port, dest_port, seq_num, ack_num, offset, flags, window, checksum, urgent_pointer = \
            struct.unpack('! H H I I 1s 1s 2s 2s 2s' , data[:20])
        offset = (offset[0] >> 4) * 4
        return src_port, dest_port, seq_num, ack_num, offset, flags, window, checksum, urgent_pointer, data[offset:]   

    def unpack_udp(self, data):
        src_port, dest_port, length, checksum = struct.unpack('! H H H H' , data[:8])
        return src_port, dest_port,length, checksum, data[8:]     

    def unpack_icmp(self, data):
        icmp_type, code, checksum = struct.unpack('! B B 2s', data[:4])
        return icmp_type, code, checksum, data[4:]
        
        
    def unpack_dns(self, data):
        qr_opcode_flags, tot_q, tot_ans, tot_auth_rr, tot_addi_rr  = struct.unpack('! 2x 2s H H H H', data[:12])
        qr = int.from_bytes(qr_opcode_flags, byteorder='big') >> 15
        opcode = (int.from_bytes(qr_opcode_flags, byteorder='big') >> 11) & 0b11101111
        return qr, opcode, tot_q, tot_ans,tot_auth_rr,tot_addi_rr, data[12:]
