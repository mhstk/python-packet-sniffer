
import struct
class Ethernet_layer:
    def __init__(self, src_mac, dest_mac, protocol, data):
        self.src_mac, self.dest_mac, self.protocol, self.data = src_mac, dest_mac, protocol, data
    
    def get_data(self):
        return self.data

    def print(self):
        print('-Ethernet layer:')
        print(f'\tsource mac: {self.src_mac}\tdestination mac: {self.dest_mac}\tip protocol: {self.protocol}')


class IPv4_layer:
    def __init__(self, version, length, fragment, ttl, protocol, header_checksum, src_ip, dest_ip, data):
        self.version, self.length, self.fragment, self.ttl, self.protocol ,\
            self.header_checksum, self.src_ip, self.dest_ip, self.data = version, length, fragment, ttl, protocol,\
                 header_checksum, src_ip, dest_ip, data
    
    def get_data(self):
        return self.data


    def is_fragmented(self):
        flagD = int((self.fragment[0] & 0b01000000) >> 6)
        return True if flagD == 0 else False

    def print(self):
        print('\t-IPV4:')
        print(f'\t\tsource ip: {self.src_ip}\tdestination ip: {self.dest_ip}\ttransport protocol: {self.protocol}')
        print(f'\t\tversion: {self.version}\theader length: {self.length}\tttl: {self.ttl}')


class TCP:
    def __init__(self, src_port, dest_port,seq_num, ack_num, offset, flags, window, checksum, urgent_pointer, data):
        self.src_port, self.dest_port, self.seq_num, self.ack_num,\
             self.offset, self.flags, self.window, self.checksum, self.urgent_pointer, self.data = \
                 src_port, dest_port, seq_num, ack_num, offset, flags, window, checksum, urgent_pointer, data


    def is_http(self):
        if self.src_port == 80 or self.dest_port == 80:
            return True
        return False
    
    def is_https(self):
        if self.src_port == 443 or self.dest_port == 443:
            return True
        return False

    def print(self):
        print('\t\t-Transport layer (TCP): ')
        print(f'\t\t\tsource port: {self.src_port}\tdestination port: {self.dest_port}')
        print(f'\t\t\tsequence number: {self.seq_num}\tack number: {self.ack_num}')
        


class UDP:
    def __init__(self, src_port, dest_port,length, checksum, data):
        self.src_port, self.dest_port, self.length, self.checksum, self.data = src_port, dest_port,length, checksum, data

    def is_dns(self):
        if self.src_port == 53 or self.dest_port == 53:
            return True
        return False
    
    
    def print(self):
        print('\t\t-Transport layer (UDP): ')
        print(f'\t\t\tsource port: {self.src_port}\tdestination port: {self.dest_port}')
        

class ICMP:
    def __init__(self, icmp_type, code, checksum, data):
        self.icmp_type, self.code, self.checksum, self.data = icmp_type, code, checksum, data

    def print(self):
        print('\t\t-ICMP:')
        print(f'\t\t\ttype: {self.icmp_type}\tcode: {self.code}\tchecksum: {self.checksum}')
        print(f'\t\t\tdata: {self.data}')


class HTTP:
    def __init__(self, raw_data, response):
        self.response = response
        self.decoded = False
        self.body = ''
        if response:
            try:
                # print('response\n\n')
                self.http_data = raw_data.decode('utf-8')
                http_datas = self.http_data.split('\r\n')
                self.version, self.status_code, self.phrase = http_datas[0].split(' ')
                self.header_dic = {}
                for i, header_key_val in enumerate(http_datas[1:]):
                    if header_key_val == '':
                        body_index = i+1
                        break
                    header_field, val = header_key_val.split(': ')
                    self.header_dic[header_field] = val
                self.body = http_datas[body_index:].join('\n')
            except:
                self.http_data = raw_data
        else:     
            try:
                # print('request\n\n')
                self.http_data = raw_data.decode('utf-8')
                http_datas = self.http_data.split('\r\n')
                self.method, self.url, self.version = http_datas[0].split(' ')
                self.header_dic = {}
                for i, header_key_val in enumerate(http_datas[1:]):
                    if header_key_val == '':
                        body_index = i+1
                        break
                    header_field, val = header_key_val.split(': ')
                    self.header_dic[header_field] = val
                self.body = http_datas[body_index:].join('\n')
            except:
                self.http_data = raw_data


        try:
            self.http_data = raw_data.decode('utf-8')
        except:
            self.http_data = raw_data
        
    def print_dic(self, dic):
        for key, val in dic.items():
            print(f'\t\t\t\t{key}: {val}')


    def print(self):
        print('\t\t\t-HTTP:')
        try:
            if not self.response:
                print(f'\t\t\t\t{self.method}')
                print(f'\t\t\t\t{self.url}')
                print(f'\t\t\t\t{self.version}')
                self.print_dic(self.header_dic)
                print(f'\t\t\t\t{self.body}')
            else:
                print(f'\t\t\t\t{self.version}')
                print(f'\t\t\t\t{self.status_code}')
                print(f'\t\t\t\t{self.phrase}')
                self.print_dic(self.header_dic)
                print(f'\t\t\t\tbody: {self.body}')
        except:
            for line in str(self.http_data).split('\n'):
                print(f'\t\t\t\t{line}')
    
class DNS:
    def __init__(self, qr, opcode, tot_q, tot_ans,tot_auth_rr, tot_addi_rr, data):
        self.qr, self.opcode, self.tot_q, self.tot_ans,self.tot_auth_rr, self.tot_addi_rr, self.data = \
            qr, opcode, tot_q, tot_ans,tot_auth_rr, tot_addi_rr, data
        self.questions = []

        try:

            i = 0
            for _ in range(self.tot_q):
                question, q_type, q_class, i =  self.read_q(self.data[i:])
            self.questions.append((question, q_type, q_class))


        except:
            pass


    def read_q(self, data):
        length = data[0]
        count = 1
        out = ''
        while length != 0:
            new = data[count:count+length].decode('utf8') + '.'
            out += new
            count += len(new)-1
            length = data[count]
            count += 1
        out = out[:-1]

        q_type, q_class = struct.unpack('! H H', data[count:count+4])

        return out, q_type, q_class, count+4


    def print(self):
        print('\t\t\t-DNS')
        print(f'\t\t\t\tQR: {self.qr}\tOpcode: {self.opcode}')
        print(f'\t\t\t\tQuestions: {self.tot_q}\tAnswers: {self.tot_ans}')
        print(f'\t\t\t\tAuthoritiy RRs: {self.tot_auth_rr}\tAdditional RRs: {self.tot_addi_rr}')
        try:
            for i in self.questions:
                print(f'\t\t\t\tQuestion: {i[0]}\tType: {i[1]}\tClass: {i[2]}')
        except:
            pass

class HTTPS:
    def __init__(self, raw_data):
        self.raw_data = raw_data

    def print(self):
        print('\t\t\t-HTTPS')


