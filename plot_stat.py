import matplotlib.pyplot as plt
from sniffer import OUTPUT_STAT

class Plot_stat:
    def __init__(self):
        self.fig = plt.figure()
        self.variables = self.read_statistic()

    def pie_chart(self, labels, sizes, name, loc):
        

        # Pie chart, where the slices will be ordered and plotted counter-clockwise:

        # fig1, ax1 = plt.subplots()
        ax1 = self.fig.add_subplot(*loc)
        self.fig.canvas.set_window_title(name)
        ax1.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=90)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

        

    def read_statistic(self):
        variables = {}
        with open(OUTPUT_STAT, 'r')as fr:
            for line in fr.readlines():
                line = line[:-1]
                if line == '': break
                var_val = line.split(': ')
                variables[var_val[0]] = int(var_val[1])
        return variables


    def ip_layer_pie(self):
        labels = ['IPV4', 'Other']
        sizes = [self.variables['ipv4']]
        sizes.append(self.variables['packets'] - sum(sizes))
        self.pie_chart(labels, sizes, 'IP', (2,2,1))


    def fragmented_layer_pie(self):
        labels = ['Fragmented', 'not']
        sizes = [self.variables['fragmented']]
        sizes.append(self.variables['packets'] - sum(sizes))
        self.pie_chart(labels, sizes, 'Fragment', (2,2,2))


    def transport_layer_pie(self):
        labels = ['ICMP', 'TCP', 'UDP', 'Other']
        sizes = [self.variables['icmp'], self.variables['tcp'], self.variables['udp']]
        sizes.append(self.variables['packets'] - sum(sizes))
        self.pie_chart(labels, sizes, 'Transport', (2,2,3))


    def application_layer_pie(self):
        labels = ['HTTP', 'HTTPS', 'DNS', 'Other']
        sizes = [self.variables['http'], self.variables['https'], self.variables['dns']]
        sizes.append(self.variables['packets'] - sum(sizes))
        self.pie_chart(labels, sizes, 'Application', (2,2,4))

if __name__ == "__main__":
    pl = Plot_stat()
    pl.ip_layer_pie()
    pl.fragmented_layer_pie()
    pl.transport_layer_pie()
    pl.application_layer_pie()
    plt.show()